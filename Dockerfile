FROM alpine

ARG BBCREDS=usr:pwd

RUN apk add --update wget && \
    rm -rf /var/cache/apk/*

RUN wget --no-check-certificate https://${BBCREDS}@bitbucket.org/thomsch98/dummy-private/raw/0e8662b4f19bb62228c622367291491954303407/Sample.txt

CMD ["echo","Hi!"]
